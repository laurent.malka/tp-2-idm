#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "mt.h"

typedef unsigned long long int ull;

extern const ull MAX_ITERATIONS;
extern const double TAB[];

double fast_pi(ull begin, ull end) {
    ull nb_of_points_inside = 0;
    for (ull i = begin * 2; i < end * 2; i += 2) {
        double x = TAB[i];
        double y = TAB[i + 1];
        if (x * x + y * y < 1) {
            ++nb_of_points_inside;
        }
    }

    return (double)nb_of_points_inside / (end - begin) * 4;
}

double slow_pi(ull iterations) {
    ull nb_of_points_inside = 0;
    for (ull i = 0; i < iterations * 2; i += 2) {
        double x = genrand_real1();
        double y = genrand_real1();
        if (x * x + y * y < 1) {
            ++nb_of_points_inside;
        }
    }

    return (double)nb_of_points_inside / iterations * 4;
}

void display_usage() {
    printf("Usage: ./exe (1|2) #iterations #repetitions\n");
    printf("1. Generate random points on the go\n");
    printf("2. Use previously generated points\n");
}

int main(int argc, char** argv) {
    if (argc < 3) {
        display_usage();
        return EXIT_FAILURE;
    }

    int method = atoi(argv[1]);
    ull iterations = atoi(argv[2]);
    ull repetitions = atoi(argv[3]);

    if (method == 1) {
        // Generation on the go
        unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
        init_by_array(init, length);

        double pi_sum = 0;
        for (ull i = 0; i < repetitions; i++) {
            double pi_estimation = slow_pi(iterations);
            printf("%-10llu: %-10f\n", i, pi_estimation);
            pi_sum += pi_estimation;
        }
        printf("Average difference: %f\n", pi_sum / repetitions - M_PI);
    } else if (method == 2) {
        // Use of already generated numbers
        if (MAX_ITERATIONS < iterations * repetitions * 2) {
            printf("Not enough random numbers generated, please generate at least %llu.\n",
                   iterations * repetitions * 2);
            return EXIT_FAILURE;
        }

        double pi_sum = 0;
        for (ull i = 0; i < repetitions; i++) {
            double pi_estimation = fast_pi(i * iterations, (i + 1) * iterations);
            printf("%-10llu: %-10f\n", i, pi_estimation);
            pi_sum += pi_estimation;
        }
        printf("Average difference: %f\n", pi_sum / repetitions - M_PI);
    } else {
        display_usage();
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
