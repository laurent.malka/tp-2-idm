.PHONY: clean

CFLAGS=-O2
# CFLAGS=-O0 -g

exe: build/data.o build/main.o build/mt.o
	- ${CC} build/data.o build/main.o build/mt.o -o exe ${CFLAGS}

build/main.o: main.c
	- ${CC} main.c -c -o build/main.o ${CFLAGS}

build/data.o: build/data.c
	- ${CC} build/data.c -c -o build/data.o ${CFLAGS}

generator: generate.c build/mt.o
	- ${CC} generate.c build/mt.o -o generator ${CFLAGS}

build/mt.o: mt.c
	- ${CC} mt.c -c -o build/mt.o ${CFLAGS}

clean:
	- rm -rf build/* generator exe
